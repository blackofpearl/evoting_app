<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vote extends Model
{
    protected $table = "vote";
    protected $fillable =['id_user','id_cdt','time_vote','status','extra_a','extra_b','extra_c'];
}
