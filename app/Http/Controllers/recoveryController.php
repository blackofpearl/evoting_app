<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\listuser;
use Illuminate\Support\Facades\Mail;
use App\Mail\iaitiMail;

class recoveryController extends Controller
{
    public function index()
    {
        return view('recovery.index');
    }
    
    public function tes1()
    {
        return view('recovery.tes1');
    }

    public function tes(Request $request)
    {
     $role = \DB::table('users')
                ->join('reg_mubes', 'reg_mubes.id_user', '=',  'users.id')
                ->select('users.email as email', 'reg_mubes.kode')
                ->where('users.email', '=', $request->email)
                ->where('reg_mubes.kode', '=', $request->kode)
                ->get();
            
                return view('vote.tes',['role'=>$role]);
    }
    public function join(Request $request)
    {
        $ceka = \DB::table('users')
            ->join('reg_mubes', 'reg_mubes.id_user', '=',  'users.id')
            ->select('users.email as email', 'reg_mubes.kode as kode','users.name as nickname', 'reg_mubes.name as fullname',
                     'reg_mubes.nim as nim')
            ->where('users.email', '=', $request->email)
            ->where('reg_mubes.kode', '=', $request->kode)
            ->where('reg_mubes.status','<','1')
            ->get();
            $ceka = $ceka->count();
    
            if ($ceka >= 1) {
                
            return redirect()->back()->withErrors(['Info', 'Status Anda waiting, silahkan tunggu atau hubungi admin']);
            }else{
                

            $cek = \DB::table('users')
            ->join('reg_mubes', 'reg_mubes.id_user', '=',  'users.id')
            ->select('users.email as email', 'reg_mubes.kode as kode','users.name as nickname', 'reg_mubes.name as fullname',
                     'reg_mubes.nim as nim')
            ->where('users.email', '=', $request->email)
            ->where('reg_mubes.kode', '=', $request->kode)
            ->get();
            $cek = $cek->count();
    
            if ($cek < 1) {
                return redirect()->back()->withErrors(['Info', 'Maaf Email/kode anda tidak kami temukan didalam system']);
            }else{
				
				$ceka = \DB::table('users')
		->select('id')
		->where('users.email', '=', $request->email)
		->get();

        foreach($ceka as $ck){

            $vt = \DB::table('vote')
            ->select('id_user')
            ->where('id_user', '=', $ck->id)
            ->get();

            $vt = $vt->count();
        if($vt > 0){
            return redirect()->back()->withErrors(['Info', 'Anda Sudah Melakukan Voting']);
        }else{
    
                $role = \DB::table('users')
                ->join('reg_mubes', 'reg_mubes.id_user', '=',  'users.id')
                ->select('users.email as email', 'reg_mubes.kode')
                ->where('users.email', '=', $request->email)
                ->where('reg_mubes.kode', '=', $request->kode)
                ->get();
            
                return view('vote.join',['role'=>$role]);
    
            }

        }

    }  

        }

    }

    public function recovery(Request $request)
    {
        $cek1 = \DB::table('users')
                ->join('reg_mubes','reg_mubes.id_user','=','users.id')
                ->where('users.email', '=', $request->email)
                ->where('reg_mubes.status','<','1')
                ->get();
        $cek1 = $cek1->count();
        
        if($cek1 > 0){

            return redirect()->back()->withErrors(['Info', 'Status Anda waiting, silahkan tunggu atau hubungi admin']);

        }else{
            function alphanumeric($panjang)
        {
                $data = array(
      
                    range(0, 9),
                    range('a', 'z'),
                    range('A','Z')
      
                );
      
                $karakter = array();
                foreach ($data as $key => $value) {
                    foreach ($value as $k => $v) {
                        $karakter[]=$v;
                    }
                }
                $data = null;
      
                for($i=0; $i<=$panjang; $i++)
                {
                    $data .=$karakter[rand($i, count($karakter)-1)];
      
                }
                return $data;
        } 
        $kode=alphanumeric(6);
    $hit = \DB::table('users')->where('email', '=', $request->email)->get();
    $hiat = $hit->count();
        
        if($hiat > 0){
        
        $role = \DB::table('users')
        ->join('reg_mubes','reg_mubes.id_user','=','users.id')
        ->join('user_roles','users.id','=','user_roles.id_user')
        ->join('roles','roles.id','=','user_roles.id_role')
        ->select('users.id as id','users.name as name','users.email as email', 'roles.role_user as role_user','user_roles.id_role as id_role'
                  , 'reg_mubes.name as fullname', 'reg_mubes.nim as nim','reg_mubes.prodi as prodi','reg_mubes.angkatan as angkatan'
                  , 'reg_mubes.file as file','reg_mubes.phone as phone', 'reg_mubes.status as status','reg_mubes.kode as code', 'reg_mubes.extra_a as extra_a','reg_mubes.extra_b as extra_b')
        ->where('users.email', '=', $request->email)
        ->get();

        foreach($role as $na)
        {
            $reg = new \App\Models\reg_mubes;
            \DB::table('reg_mubes')->where('id_user',$na->id)->update([
               'kode' => $kode
                ]);

            $details = [
                'title' => 'Recovery Success',
                'body' => 'Recovery kode sudah dalam posisi terupdate',
                'username' => $na->name,
                'nim' => $na->nim,
                'kode' => $kode
                 ];
        
                Mail::to($request->email)->send(new iaitiMail($details));
        
        return redirect('/recovery')->with('success', 'Code Sudah Di Update, Silahkan Cek Email anda');
        }

        }else{
        
            return redirect()->back()->withErrors(['Info', 'Email Anda Tidak Ditemukan, Silahkan hubungi admin']);  
        
        }
        }

        }

        public function vote(Request $request)
        {
            $hit = \DB::table('vote')->where('id_user', '=', $request->id_user)->get();
            $hit = $hit->count();

            if($hit > 0){
                return redirect()->back()->withErrors(['Info', 'Anda Sudah Melakukan e_voting']);
            }else{
                date_default_timezone_set("Asia/Jakarta");
            $date = date('Y-m-d H:i:s');
            
            $reg1 = \DB::table('ip_ceck')
			->select('id')
			->where(\DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),'=',date('Y-m-d'))
			->where('ip','=',$request->ip)
			->get();

            $vote = new \App\Models\vote;
            $vote->id_user = $request->id_user;
            $vote->id_cdt = $request->id_cdt;
            $vote->time_vote = $date;
            $vote->save();

			
            $vote = new \App\Models\ip_cek;
            $vote->ip = $request->ip;
            $vote->user = $request->id_user;
            $vote->position = 'VOTING';
            $vote->date_ip = date("d-M-y H:m:s");
            $vote->status = count($reg1)+1;
            $vote->save();   
            
            $role = \DB::table('users')
            ->select('users.email as email','vote.time_vote as times')
            ->join('vote','vote.id_user','=','users.id')
            ->where('users.id', '=', $request->id_user)
            ->get();

            foreach($role as $naa)
            {
            
                /*
                $details = [
                'title' => 'E-VOTING SUCCESS',
                'body' => 'Terima Kasih Telah Melakukan E_voting',
                'username' => $naa->name,
                'nim' => '-',
                'kode' => '-'
                 ];
        
                Mail::to($naa->email)->send(new iaitiMail($details));
                 */
                 ?>
                 <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'><link rel="stylesheet" href="./style.css">
<style>
.main-content{
	width: 50%;
	border-radius: 20px;
	box-shadow: 0 5px 5px rgba(0,0,0,.4);
	margin: 5em auto;
	display: flex;
}
.company__info{
	background-color: #008080;
	border-top-left-radius: 20px;
	border-bottom-left-radius: 20px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	color: #fff;
}
.fa-android{
	font-size:3em;
}
@media screen and (max-width: 640px) {
	.main-content{width: 90%;}
	.company__info{
		display: none;
	}
	.login_form{
		border-top-left-radius:20px;
		border-bottom-left-radius:20px;
	}
}
@media screen and (min-width: 642px) and (max-width:800px){
	.main-content{width: 70%;}
}
.row > h2{
	color:#008080;
}
.login_form{
	background-color: #fff;
	border-top-right-radius:20px;
	border-bottom-right-radius:20px;
	border-top:1px solid #ccc;
	border-right:1px solid #ccc;
}
form{
	padding: 0 2em;
}
.form__input{
	width: 100%;
	border:0px solid transparent;
	border-radius: 0;
	border-bottom: 1px solid #aaa;
	padding: 1em .5em .5em;
	padding-left: 2em;
	outline:none;
	margin:1.5em auto;
	transition: all .5s ease;
}
.form__input:focus{
	border-bottom-color: #008080;
	box-shadow: 0 0 5px rgba(0,80,80,.4); 
	border-radius: 4px;
}
.btn{
	transition: all .5s ease;
	width: 70%;
	border-radius: 30px;
	color:#008080;
	font-weight: 600;
	background-color: #fff;
	border: 1px solid #008080;
	margin-top: 1.5em;
	margin-bottom: 1em;
}
.btn:hover, .btn:focus{
	background-color: #008080;
	color:#fff;
}
body {
        margin: 0;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 20px;
        color: #333;
        background-color: #fff;
        padding-left: 15px;
            }
            
            .btn {
                background-color: #04AA6D; /* Green */
                border: none;
                color: white;
                padding: 6px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 14px;
                margin: 4px 2px;
                cursor: pointer;
                }

                .button1 {border-radius: 2px;}
                .button2 {border-radius: 4px;}
                .button3 {border-radius: 8px;}
                .button4 {border-radius: 12px;}
                .button5 {border-radius: 50%;}
            
            .btn-default {
                color: #333;
                background-color: #fff;
                border-color: #ccc;
            }
            
            h1{
                font-size: 36px;
            }
            
            h1{
                font-family: inherit;
                font-weight: 500;
                line-height: 1.1;
                color: inherit;
            }
            
            h1 small{
                font-size: 65%;
            }
            
            h1 small {
                font-weight: 400;
                line-height: 1;
                color: #777;
                display: block;
            padding-top: 15px;
            }
            .specific{
                background-color: #fff;
            }

        p a{
        padding: 5px;
        }
</style>

    <link href="{{ asset('assets/img/Logo-IAITI.png') }}" rel="icon">
    <link href="{{ asset('assets/img/Logo-IAITI.png') }}" rel="apple-touch-icon">
               
                    <div id="close">                
                        <div class="container-popup">
                            <div class="specific">
                            <div class="container-fluid">
                            <div class="row main-content bg-success text-center">
                                <div class="col-md-2 text-center company__info">
                                    <span class="company__logo"><h2><span class="fa fa-envelope"></span></h2></span>
                                    <h6 class="company_title">Ikatan Alumni Institut Teknologi Indonesia</h6>
                                </div>
                                <div class="col-md-10 col-xs-12 col-sm-12 login_form ">
                                    <div class="container-fluid">
                                        <div class="row">
                                             <img src="assets/img/Logo-Mubes3.png" width='70%'>
                                            <h2> 
                                            <?php
                                            $mubes = \DB::table('mubes')
                                            ->select('*')
                                            ->get();
                                            
                                            foreach ($mubes as $mbs){
                                                echo $mbs->title;
                                            }
                                            ?>
                                            </h2>
                                        </div>
                    
                                        <div class="row">

                                                <div class="row">
                                                    
                                                <h2>HELLO, <?php echo $naa->email; ?> </h2>
                                                <h3>e-voting Success</h3>
                                                <p>Terima Kasih, Telah Melakukan e-voting
                                                    <br/>Pada : <?php echo $naa->times; ?>
                                                </p>
                                                <p>Thank You</p>
                                                    
                                                </div>
                                                
                                            <a class="btn btn-primary mr-3" href="/">Kembali</a>
                                            <p>Untuk Pemenuhan pembuktian bahwa telah melakukan e-voting, Silahkan lakukan SCREENSHOT!!, Atau silahkan klik tombol dibawah ini!</p>
                                            <button type="button" class="btn btn-primary mr-2">Take a Screenshot!</button>
                                            <script>
                                            document.querySelector('button').addEventListener('click', function() {
                                                html2canvas(document.querySelector('.specific'), {
                                                    onrendered: function(canvas) {
                                                        // document.body.appendChild(canvas);
                                                    return Canvas2Image.saveAsPNG(canvas);
                                                    }
                                                });
                                            });
                                            </script>
                                            <script src='https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js'></script>
                                            <script src='https://superal.github.io/canvas2image/canvas2image.js'></script> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>                    
                    </div>
                    </div>
                 <?php
            }
            
           // return redirect('/')->with('success', 'Voting Success!, Terima Kasih Telah Melakukan Voting');
            }
                
        }

        public function quickcount()
        {
            return view('vote.quickcount');
        }
}