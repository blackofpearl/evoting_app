<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>IA-ITI</title>
  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('assets/js/app.js') }}" defer></script>

    <!--favicon-->
    <link href="{{ asset('assets/img/Logo-IAITI.png') }}" rel="icon">
    <link href="{{ asset('assets/img/Logo-IAITI.png') }}" rel="apple-touch-icon">
  
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
</head>
<body>
<!-- partial:index.partial.html -->

<section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        
		<div class="row">
         
          <div class="col-md-12">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <p class="text-center">
                      <strong><?php /*
            $role = \DB::table('mubes')
            ->select('title')
            ->get();
            
            foreach($role as $na){
              echo $na->title;
            }

            ?> : 1 juli, 2020 - 30 Ags, 2020</strong>
            */ ?>
                    </p>

                    <div class="chart">

                    <?php

                    $kl = \DB::table('candidate')
                    ->join('schedule','schedule.id','=','candidate.id_sch')
                    ->join('reg_mubes','reg_mubes.id','=','candidate.id_alumni')
                    ->select('reg_mubes.name as nama','candidate.id as id')
                    ->where('schedule.status','<>','2')
                    ->get();
                    ?>
                    @foreach($kl as $l)
                    <div class="progress-group">
                     <h4><b> {{$l->nama}} </b></h4>
                      <span class="float-right"><b><h4>
                      <?php
                        $ttl = \DB::table('vote')
                        ->select('id_cdt')
                        ->where('id_cdt', '=', $l->id)
                        ->get();
                        $t=count($ttl);
                        echo count($ttl);
                        ?>
                      </b>/
                        <?php
                        $role = \DB::table('reg_mubes')
                        ->select('nim')
                        ->whereBetween('status', array(1, 2))
                        ->get();
                        $c=count($role);
                        echo count($role); 
                        ?></h4>
                      </span>
                      <div class="progress progress-sm">
                        <div class="progress-bar bg-yellow" style="width:<?php echo $t/$c*100; ?>%"></div>
                      </div>
                    </div>
                    @endforeach
                    <h4><b><a href="javascript:history.back()">Kembali</a></b></h4>
                    </div>
                    <!-- /.chart-responsive -->
                  </div>
                 
                  
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
      </div><!--/. container-fluid -->
    </section>
  
	<center>
      <div class="container-fluid">
        <!-- Info boxes -->
		<div class="row">
          	<div class="col-md-12">
            	<div class="card-body">
				
				<!-- partial:index.partial.html -->
				<div class="pure-g">
					<div class="pure-u-1-3">
					<div id="stacked-bar-3"></div>
					</div>
				</div>
				</div> <!-- info-table-wrapper -->
			<!-- partial -->
			</div>
		</div>
	   </div>
			
			</center>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://code.highcharts.com/highcharts.js'></script>
<script>
var $j = jQuery.noConflict();
$j(function () {
    $j('#stacked-bar-3').highcharts({
  title: {
    text: 'e_voting IA-ITI'
  },
  xAxis: {
    categories: [
	<?php
					$reas = \DB::table('prodi')
						->get();
					?>
					@foreach($reas as $roa)
						'{{$roa->name}}',
					@endforeach
	]
  },
  labels: {
    items: [{
      html: 'e_voting',
      style: {
        left: '50px',
        top: '18px',
        color: ( // theme
          Highcharts.defaultOptions.title.style &&
          Highcharts.defaultOptions.title.style.color
        ) || 'black'
      }
    }]
  },
  series: [{
    type: 'column',
    name: 'Sudah Voting',
    data: [
		<?php
		$reas = \DB::table('prodi')
		->get();
	?>
	@foreach($reas as $roa)
					<?php
					$reass = \DB::table('vote')
						->join('reg_mubes', 'reg_mubes.id_user', '=', 'vote.id_user')
						->select(DB::raw('COUNT(reg_mubes.id_user) as hit'))
						->where('reg_mubes.prodi','=', $roa->name)
						->get();
					?>
					@foreach($reass as $roas)
						{{$roas->hit}},
					@endforeach
		
	@endforeach
	]
  }, {
    type: 'column',
    name: 'Belum Voting',
    data: [
		<?php
		$reas = \DB::table('prodi')
		->get();
	?>
	@foreach($reas as $roa)
	<?php
					$bv = \DB::table('reg_mubes')
					->select(\DB::raw('COUNT(reg_mubes.id_user) as hit'))
					->leftJoin('vote','vote.id_user','=','reg_mubes.id_user')
					->whereNull('vote.id_user')
					->where('reg_mubes.prodi','=', $roa->name)
					->get();
					?>
					@foreach($bv as $bvo)
					{{$bvo->hit}},
					@endforeach	
		@endforeach
	]
  }, {
    type: 'spline',
    name: 'DPT Approved',
    data: [
		<?php
		$prodi = \DB::table('prodi')
		->get();
		?>
		@foreach($prodi as $pd)
					<?php
					$rm = \DB::table('reg_mubes')
						->select(DB::raw('COUNT(id_user) as hit'))
						->where('prodi','=', $pd->name)
						->get();
					?>
					@foreach($rm as $rmg)
						{{$rmg->hit}},
					@endforeach
		
	@endforeach
	],
    marker: {
      lineWidth: 2,
      lineColor: Highcharts.getOptions().colors[3],
      fillColor: 'white'
    }
  }, ]
});
  
});
</script>
</body>
</html>