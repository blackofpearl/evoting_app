<!DOCTYPE html>
<html lang="en" >
<head>
		<?php
		$userIp = Request::ip();
		$reg1 = \DB::table('ip_ceck')
			->select('id')
			->where(\DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),'=',date('Y-m-d'))
			->where('ip','=',$userIp)
			->get();
		$totvote = count($reg1);
		?>
  <meta charset="UTF-8">
  <script>
$.ajaxSetup({
 headers: {
  ‘X-CSRF-TOKEN’: $(‘meta[name=”csrf-token”]’).attr(‘content’)
 }
});
</script>
  <meta name=”csrf-token” content=”{{ csrf_token() }}”>
  <title>IA-ITI</title>
  	<link href="{{ URL::asset('assets/img/Logo-IAITI.png') }}" rel="icon">
  	<link href="{{ URL::asset('assets/img/Logo-IAITI.png') }}" rel="apple-touch-icon">
  	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
	<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
	<link rel="stylesheet" href="{{ URL::asset('sss.css') }}">
	<link href="{{ asset('assets/img/Logo-IAITI.png') }}" rel="icon">
    <link href="{{ asset('assets/img/Logo-IAITI.png') }}" rel="apple-touch-icon">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Yinka Enoch Adedokun">
    <link href="{{ asset('assets/css/bar/style.css') }}" rel="stylesheet">
	<title>Login Vote Page</title>
	<style>
.main-content{
	width: 50%;
	border-radius: 20px;
	box-shadow: 0 5px 5px rgba(0,0,0,.4);
	margin: 5em auto;
	display: flex;
}
.company__info{
	background-color: #008080;
	border-top-left-radius: 20px;
	border-bottom-left-radius: 20px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	color: #fff;
}
.fa-android{
	font-size:3em;
}
@media screen and (max-width: 640px) {
	.main-content{width: 90%;}
	.company__info{
		display: none;
	}
	.login_form{
		border-top-left-radius:20px;
		border-bottom-left-radius:20px;
	}
}
@media screen and (min-width: 642px) and (max-width:800px){
	.main-content{width: 70%;}
}
.row > h2{
	color:#008080;
}
.login_form{
	background-color: #fff;
	border-top-right-radius:20px;
	border-bottom-right-radius:20px;
	border-top:1px solid #ccc;
	border-right:1px solid #ccc;
}
form{
	padding: 0 2em;
}
.form__input{
	width: 100%;
	border:0px solid transparent;
	border-radius: 0;
	border-bottom: 1px solid #aaa;
	padding: 1em .5em .5em;
	padding-left: 2em;
	outline:none;
	margin:1.5em auto;
	transition: all .5s ease;
}
.form__input:focus{
	border-bottom-color: #008080;
	box-shadow: 0 0 5px rgba(0,80,80,.4); 
	border-radius: 4px;
}
.btn{
	transition: all .5s ease;
	width: 70%;
	border-radius: 30px;
	color:#008080;
	font-weight: 600;
	background-color: #fff;
	border: 1px solid #008080;
	margin-top: 1.5em;
	margin-bottom: 1em;
}
.btn:hover, .btn:focus{
	background-color: #008080;
	color:#fff;
}

	body {
  padding: 50px;  
}

.progress-bg {
	margin: 0 auto;
	width: 65%;
	height: 78px;
	border-radius: 10px;
  text-align: center;
	-moz-box-shadow:    inset 0 0 10px #ccc;
	-webkit-box-shadow: inset 0 0 10px #ccc;
	box-shadow:         inset 0 0 10px #ccc;
}

.progress-bar {
	height: 78px;
	border-radius: 10px;
	float: left;
	width: 50%;
	/* fallback */ 
	background-color: #1c314a; 
	
	/* Safari 4-5, Chrome 1-9 */ 
	background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#1c314a), to(#27425f)); 
	
	/* Safari 5.1, Chrome 10+ */ 
	background: -webkit-linear-gradient(top, #1c314a, #27425f); 
	
	/* Firefox 3.6+ */ 
	background: -moz-linear-gradient(top, #1c314a, #27425f); 
	
	/* IE 10 */ 
	background: -ms-linear-gradient(top, #1c314a, #27425f); 
	
	/* Opera 11.10+ */ 
	background: -o-linear-gradient(top, #1c314a, #27425f);	
}

.progress-bg h3.goal, .progress-bg h3.raised {
	font-family: Arial,sans-serif;
  font-size: 2em;
  font-weight: 600;
	line-height: 78px;
	margin: 0;
	padding: 0;
	text-align: center;
	display: inline;
}


.progress-bg h3.raised {
	color: #fff;
	margin: 14px 25px 0 50px;
	padding: 0 25px 0 0;
}

.progress-bg h3.goal {
	color: #b2b2b2;
  text-align: center;
}

body .progress-bg h3.raised {
    -webkit-animation: fadein 4s; /* Safari and Chrome */
       -moz-animation: fadein 4s; /* Firefox */
        -ms-animation: fadein 4s; /* Internet Explorer */
         -o-animation: fadein 4s; /* Opera */
            animation: fadein 4s;
}

@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

/* Firefox */
@-moz-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

/* Safari and Chrome */
@-webkit-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

/* Internet Explorer */
@-ms-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}​

/* Opera */
@-o-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}​

.progress-bg h3.goal {
	float: right;
	display: inline;
	padding: 0 25px 0 0;
  text-align: center;
}

body .progress-bg div {
	-webkit-animation: progress-bar 2s ease forwards;
	-moz-animation: progress-bar 2s ease forwards;
	-o-animation: progress-bar 2s ease forwards;
	animation: progress-bar 2s ease forwards;
}

@-webkit-keyframes progress-bar {
    from { width: 0%; }
    to { width: 50%; }
}

@-moz-keyframes progress-bar {
    from { width: 0%; }
    to { width: 50%; }
}

@-o-keyframes progress-bar {
    from { width: 0%; }
    to { width: 50%; }
}

@keyframes progress-bar {
    from { width: 0%; }
    to { width: <?php echo $totvote*2; ?>0%; }
}
</style>
</head>
<body>
	<!-- Main Content -->
	<center>
	<div class="alert alert-success">
	<h4> IP address anda sudah di gunakan {{$totvote}} kali, sistem hanya memperkenankan maksimum 3 x login dengan menggunakan IP yang sama selama 24 jam kedepan</h4>
	</div>
	<div class="progress-bg">
    	<div class="progress-bar">
        	<h3 class="raised">{{$totvote}}&nbsp;x</h3>
        </div>
    </div>
	</center>

	<div class="container-fluid">
	<?php
                  date_default_timezone_set("Asia/Jakarta");
                  $tes = \DB::table('schedule')
                    ->select('periode_a','periode_b')
					->where('id','=','5')
                    ->get();
                  ?>
				  
				@foreach ($tes as $t)
	
					 <?php 
					 if(date('M d, Y H:i:s') > date("M d, Y H:i:s", strtotime($t->periode_a)) && date('M d, Y H:i:s') < date("M d, Y H:i:s", strtotime($t->periode_b)))
					 {
					 ?>
		<div class="row main-content bg-success text-center">
			<div class="col-md-2 text-center company__info">
				<img src="assets/img/Logo-IAITI.png" class="img-fluid" witdh='10%'>
				<h6 class="company_title">Ikatan Alumni Institut Teknologi Indonesia</h6>
			</div>
			<div class="col-md-10 col-xs-12 col-sm-12 login_form ">
				<div class="container-fluid">
					<div class="row">
						<h2>
						Anda diluar waktu pemilihan 
                        </h2>
						<br/>
                        <style>
            .close {
            cursor: pointer;
            position: absolute;
            padding: 12px 16px;
            transform: translate(0%, -50%);
            }
        </style>
        @if(count($errors) > 0)
				<div class="alert alert-danger">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
                </center>
				</div>
            	@endif

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
                            {!! \Session::get('success') !!}</li>
                </center>
                    </div>
                @endif
                
                <script>
                var closebtns = document.getElementsByClassName("close");
                var i;

                for (i = 0; i < closebtns.length; i++) {
                closebtns[i].addEventListener("click", function() {
                    this.parentElement.style.display = 'none';
                });
                }
                </script>
					</div>
					
					<div class="row">
						<p>Lupa Kode anda..? <a href="/recovery">Pemulihan Kode</a></p>
					</div>
				</div>
			</div>
			
		</div>
		<?php
					 }else{
		?>
		<div class="row main-content bg-success text-center">
			<div class="col-md-2 text-center company__info">
				<img src="assets/img/Logo-IAITI.png" class="img-fluid" witdh='10%'>
				<h6 class="company_title">Ikatan Alumni Institut Teknologi Indonesia</h6>
			</div>
			<div class="col-md-10 col-xs-12 col-sm-12 login_form ">
				<div class="container-fluid">
					<div class="row">
						<h2>LogIn
                        <?php
                        $mubes = DB::table('mubes')
                        ->select('*')
                        ->get();
                        ?>
                        @foreach ($mubes as $mbs)
                            {{$mbs->title}}
                        @endforeach
                        </h2>
						
                        <style>
            .close {
            cursor: pointer;
            position: absolute;
            padding: 12px 16px;
            transform: translate(0%, -50%);
            }
        </style>
        @if(count($errors) > 0)
				<div class="alert alert-danger">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
                </center>
				</div>
            	@endif

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                <span class="close"><h3><b>&times;</b></h3></span>
                <center>
                            {!! \Session::get('success') !!}</li>
                </center>
                    </div>
                @endif
                
                <script>
                var closebtns = document.getElementsByClassName("close");
                var i;

                for (i = 0; i < closebtns.length; i++) {
                closebtns[i].addEventListener("click", function() {
                    this.parentElement.style.display = 'none';
                });
                }
                </script>
					</div>
					<div class="row">
                        <form class="form" action="<?php echo url("/vote/join"); ?>" method="post">
                        @csrf
							<div class="row">
								<input type="email" name="email" id="email" class="form__input" placeholder="email" required>
							</div>
							<div class="row">
								<!-- <span class="fa fa-lock"></span> -->
								<input type="text" name="kode" id="kode" class="form__input" placeholder="kode" required>
							</div>
							
							<div class="row">
								<input type="submit" value="Join Vote" class="btn">
							</div>
						</form>
					</div>
					<div class="row">
						<a href="/realcount">Real Count</a></p>
						<p>Lupa Kode anda..? <a href="/recovery">Pemulihan Kode</a></p>
					</div>
				</div>
			</div>
			
		</div>	
		<?php
		 }
		?>
		
		@endforeach
		<style type="text/css">
		.main {
         text-align: center;
         color: #696969;
     }
     #clock > div {
         border-radius: 5px;
         background: #ffa500;
         padding: 5px 5px 5px 5px;
         color: #fff;
         display: inline-block;
     }
     #clock > div > p {
         font-size: 20px;
     }
     #days, #hours, #minutes, #seconds {
         text-align: center;
         padding: 20px;
         font-size: 20px;
         background: #fb8e00;
         width: 40px;
     }
     @keyframes turn {
         0% {transform: rotateX(0deg)}
         100% {transform: rotateX(360deg)}
     }
</style>
<?php
                  date_default_timezone_set("Asia/Jakarta");
                  $rolesaa = \DB::table('schedule')
                    ->select('periode_a','periode_b')
					->where('id','=','4')
                    ->get();
                  ?>
				@foreach ($rolesaa as $na)
	
					 @if(date('M d, Y H:i:s') <= date("M d, Y H:i:s", strtotime($na->periode_a))) 
					 <center>
						 <h4><b>e-Voting Resmi Akan di Buka pada : {{date("M d, Y H:i:s", strtotime($na->periode_a))}}</b></h4><hr/>
					 </center> 
					 <center>		
		<div class="main">
        <div id="clock">
            <div><span id="days"></span><p>Hari</p></div>
            <div><span id="hours"></span><p>Jam</p></div>
            <div><span id="minutes"></span><p>Menit</p></div>
            <div><span id="seconds"></span><p>Detik</p></div>
        </div>
    </div>
	<br/>
	</center>
	<script type="text/javascript">
     function animation(span) {
         span.className = "turn";
         setTimeout(function () {
             span.className = ""
         }, 700);
     }
 
     function Countdown() {
   
         setInterval(function () {
 
            var hari    = document.getElementById("days");
            var jam     = document.getElementById("hours");
            var menit   = document.getElementById("minutes");
            var detik   = document.getElementById("seconds");
               
            var deadline    = new Date("<?php echo date("M d, Y H:i:s", strtotime($na->periode_a)); ?>");
            var waktu       = new Date();
            var distance    = deadline - waktu;
               
            var days    = Math.floor((distance / (1000*60*60*24)));
            var hours   = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
               
            if (days < 10)
            {
               days = '0' + days;
            }
            if (hours < 10)
            {
               hours = '0' + hours;
            }
            if (minutes < 10)
            {
               minutes = '0' + minutes;
            }
            if (seconds < 10)
            {
               seconds = '0' + seconds;
            }
 
            hari.innerHTML    = days;
            jam.innerHTML     = hours;
            menit.innerHTML   = minutes;
            detik.innerHTML   = seconds;
            //animation
            animation(detik);
            if (seconds == 0) animation(menit);
            if (minutes == 0) animation(jam);
            if (hours == 0) animation(hari);
 
         }, 1000);
     }
 
     Countdown();
 
</script>
					@endif

					@if(date('M d, Y H:i:s') <= date("M d, Y H:i:s", strtotime($na->periode_b)) && date('M d, Y H:i:s') > date("M d, Y H:i:s", strtotime($na->periode_a)) ) 
					 <center>
						 <h4><b>e-Voting Resmi Akan di Tutup pada : {{date("M d, Y H:i:s", strtotime($na->periode_b))}}</b></h4><hr/>
					 </center> 
					 <center>		
		<div class="main">
        <div id="clock">
            <div><span id="days"></span><p>Hari</p></div>
            <div><span id="hours"></span><p>Jam</p></div>
            <div><span id="minutes"></span><p>Menit</p></div>
            <div><span id="seconds"></span><p>Detik</p></div>
        </div>
    </div>
	<br/>
	</center>
	<script type="text/javascript">
     function animation(span) {
         span.className = "turn";
         setTimeout(function () {
             span.className = ""
         }, 700);
     }
 
     function Countdown() {
   
         setInterval(function () {
 
            var hari    = document.getElementById("days");
            var jam     = document.getElementById("hours");
            var menit   = document.getElementById("minutes");
            var detik   = document.getElementById("seconds");
               
            var deadline    = new Date("<?php echo date("M d, Y H:i:s", strtotime($na->periode_b)); ?>");
            var waktu       = new Date();
            var distance    = deadline - waktu;
               
            var days    = Math.floor((distance / (1000*60*60*24)));
            var hours   = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
               
            if (days < 10)
            {
               days = '0' + days;
            }
            if (hours < 10)
            {
               hours = '0' + hours;
            }
            if (minutes < 10)
            {
               minutes = '0' + minutes;
            }
            if (seconds < 10)
            {
               seconds = '0' + seconds;
            }
 
            hari.innerHTML    = days;
            jam.innerHTML     = hours;
            menit.innerHTML   = minutes;
            detik.innerHTML   = seconds;
            //animation
            animation(detik);
            if (seconds == 0) animation(menit);
            if (minutes == 0) animation(jam);
            if (hours == 0) animation(hari);
 
         }, 1000);
     }
 
     Countdown();
 
</script>
					@endif
					 

@endforeach
	</div>
	
	<center>
      <div class="container-fluid">
        <!-- Info boxes -->
		<div class="row">
          	<div class="col-md-12">
            	<div class="card-body">
				
				<!-- partial:index.partial.html -->
				<div class="pure-g">
					<div class="pure-u-1-3">
					<div id="stacked-bar-3"></div>
					</div>
				</div>
				</div> <!-- info-table-wrapper -->
			<!-- partial -->
			</div>
		</div>
	   </div>
			
			</center>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://code.highcharts.com/highcharts.js'></script>
<script>
var $j = jQuery.noConflict();
$j(function () {
    $j('#stacked-bar-3').highcharts({
  title: {
    text: 'e_voting IA-ITI'
  },
  xAxis: {
    categories: [
	<?php
					$reas = \DB::table('prodi')
						->get();
					?>
					@foreach($reas as $roa)
						'{{$roa->name}}',
					@endforeach
	]
  },
  labels: {
    items: [{
      html: 'e_voting',
      style: {
        left: '50px',
        top: '18px',
        color: ( // theme
          Highcharts.defaultOptions.title.style &&
          Highcharts.defaultOptions.title.style.color
        ) || 'black'
      }
    }]
  },
  series: [{
    type: 'column',
    name: 'Sudah Voting',
    data: [
		<?php
		$reas = \DB::table('prodi')
		->get();
	?>
	@foreach($reas as $roa)
					<?php
					$reass = \DB::table('vote')
						->join('reg_mubes', 'reg_mubes.id_user', '=', 'vote.id_user')
						->select(DB::raw('COUNT(reg_mubes.id_user) as hit'))
						->where('reg_mubes.prodi','=', $roa->name)
						->get();
					?>
					@foreach($reass as $roas)
						{{$roas->hit}},
					@endforeach
		
	@endforeach
	]
  }, {
    type: 'column',
    name: 'Belum Voting',
    data: [
		<?php
		$reas = \DB::table('prodi')
		->get();
	?>
	@foreach($reas as $roa)
	<?php
					$bv = \DB::table('reg_mubes')
					->select(\DB::raw('COUNT(reg_mubes.id_user) as hit'))
					->leftJoin('vote','vote.id_user','=','reg_mubes.id_user')
					->whereNull('vote.id_user')
					->where('reg_mubes.prodi','=', $roa->name)
					->get();
					?>
					@foreach($bv as $bvo)
					{{$bvo->hit}},
					@endforeach	
		@endforeach
	]
  }, {
    type: 'spline',
    name: 'DPT Approved',
    data: [
		<?php
		$prodi = \DB::table('prodi')
		->get();
		?>
		@foreach($prodi as $pd)
					<?php
					$rm = \DB::table('reg_mubes')
						->select(DB::raw('COUNT(id_user) as hit'))
						->where('prodi','=', $pd->name)
						->get();
					?>
					@foreach($rm as $rmg)
						{{$rmg->hit}},
					@endforeach
		
	@endforeach
	],
    marker: {
      lineWidth: 2,
      lineColor: Highcharts.getOptions().colors[3],
      fillColor: 'white'
    }
  }, ]
});
  
});
</script>
</body>
</html>