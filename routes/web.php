<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('vote.index');
});

Route::get('/recovery', [App\Http\Controllers\recoveryController::class, 'index'])->name('index');
Route::post('/vote/join', [App\Http\Controllers\recoveryController::class, 'join'])->name('join');
Route::post('/recovery/send', [App\Http\Controllers\recoveryController::class, 'recovery'])->name('recovery');
Route::post('/vote', [App\Http\Controllers\recoveryController::class, 'vote'])->name('vote');
Route::get('/realcount', [App\Http\Controllers\recoveryController::class, 'quickcount'])->name('vote');
Route::post('/tes', [App\Http\Controllers\recoveryController::class, 'tes'])->name('tes');
Route::get('/tes1', [App\Http\Controllers\recoveryController::class, 'tes1'])->name('tes1');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
